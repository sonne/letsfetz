package com.htwk.lfischer.letsfetz.logic;

import android.util.Log;

import com.htwk.lfischer.letsfetz.model.Event;
import com.htwk.lfischer.letsfetz.model.ResponseEvent;
import com.htwk.lfischer.letsfetz.network.EventService;

import java.util.LinkedList;
import java.util.List;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Laura on 08.01.2017.
 */

public class EventsLogic {

    public static List<Event> loadWithRetrofit(String city, String date) throws Exception {
        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit = builder.baseUrl("http://api.eventful.com/json/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        String dateRange = date + "-" + date;
        EventService eventService = retrofit.create(EventService.class);
        Response<ResponseEvent> response = null;
        try {
            response = eventService.getEvents(city, dateRange, "100").execute();
        } catch (Exception e) {
            Log.e("Keine Response", String.valueOf(e));
            return new LinkedList<>();
        }
        if(response.isSuccessful()) {
            return response.body().getEvents().getEvent();
        } else {
            Log.i("Fehler in EventsLogic", response.errorBody().toString());
            return new LinkedList<>();
        }
    }

}
