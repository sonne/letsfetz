package com.htwk.lfischer.letsfetz.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.htwk.lfischer.letsfetz.async.EventLoader;
import com.htwk.lfischer.letsfetz.util.OnItemClickListener;
import com.htwk.lfischer.letsfetz.R;
import com.htwk.lfischer.letsfetz.model.Event;
import com.htwk.lfischer.letsfetz.ui.adapter.EventAdapter;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Laura on 16.01.2017.
 */

public class SecondActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Event>>, OnItemClickListener<Event>{
    private RecyclerView eventsRecyclerView;
    private RecyclerView.Adapter evt_adapter;
    private String city;
    private String date;
    private String queryDate;
    private List<Event> eventList = new LinkedList<>();
    public String bundleName = FavoriteActivity.bundleName;
    private ProgressDialog progDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        eventsRecyclerView = (RecyclerView) findViewById(R.id.eventsRecycler);
        eventsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        evt_adapter = new EventAdapter(eventList, this);
        eventsRecyclerView.setAdapter(evt_adapter);
        getBundleInformation();
        getSupportLoaderManager().restartLoader(0, null, this);
    }

    private void getBundleInformation() {
        Bundle bundle = getIntent().getBundleExtra("parameters");
        if (bundle != null){
            city =  bundle.getString("city");
            date = bundle.getString("date");
            TextView dateView = (TextView) findViewById(R.id.dateView);
            TextView cityView = (TextView) findViewById(R.id.cityView);
            dateView.setText(date);
            cityView.setText(city);
            String queryDateArray[] = date.split("\\.");
            queryDate = queryDateArray[2]+queryDateArray[1]+queryDateArray[0]+"00";
            eventsRecyclerView.setAdapter(new EventAdapter(eventList, this));
        }
    }

    @Override
    public void onItemClick(Event item) {
        Intent intent = new Intent(this, EventActivity.class);
        Bundle bundle = new Bundle();
        if (item.getImage() != null){
            bundle.putString("imageUrl", item.getImage().getMedium().getUrl());
        } else {
            bundle.putString("imageUrl", "");
        }
        bundle.putParcelable("event", item);
        intent.putExtra(bundleName, bundle);
        startActivity(intent);
    }

    @Override
    public Loader<List<Event>> onCreateLoader(int id, Bundle args) {
        progDialog = new ProgressDialog(this);
        progDialog.setMessage("Events werden geladen...");
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.show();
        return new EventLoader(this, city, queryDate);
    }

    @Override
    public void onLoadFinished(Loader<List<Event>> loader, List<Event> data) {
        progDialog.hide();
        if (data.size() > 0){
            Collections.sort(data);
            evt_adapter = new EventAdapter(data, this);
            eventsRecyclerView.setAdapter(evt_adapter);
        } else {
            Toast.makeText(this, "Leider keine Events", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onLoaderReset(Loader<List<Event>> loader) {

    }
}
