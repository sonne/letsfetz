package com.htwk.lfischer.letsfetz.logic;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.htwk.lfischer.letsfetz.model.Event;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by Laura on 21.01.2017.
 */

public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DB_NAME = "events.db";
    private static final int DB_VERSION = 1;

    private Dao<Event, Integer> eventDao;

    public DBHelper(Context context) {
        super(context,DB_NAME,null,DB_VERSION);
    }

    public Dao<Event, Integer> getEventDao(){
        try{
            if (eventDao == null){
                eventDao = getDao(Event.class);
            }
            return eventDao;
        } catch (SQLException e) {
            Log.e("Problem in getEventDao", e.getMessage(), e);
            return null;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Event.class);
        } catch (SQLException e) {
            Log.e("Problem in onCreate", e.getMessage(), e);;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }
}
