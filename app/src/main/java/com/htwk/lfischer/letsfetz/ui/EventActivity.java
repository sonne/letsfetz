package com.htwk.lfischer.letsfetz.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.htwk.lfischer.letsfetz.logic.DBHelper;
import com.htwk.lfischer.letsfetz.R;
import com.htwk.lfischer.letsfetz.model.Event;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

public class EventActivity extends AppCompatActivity {
    private Event event;
    private ImageButton star;
    private Boolean isChecked;
    private String imageUrl;
    public static String mapsBundle = "coordinates";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        String bundleName = FavoriteActivity.bundleName;
        star = (ImageButton) findViewById(R.id.starButton);
        Bundle bundle = getIntent().getBundleExtra(bundleName);
        if (bundle != null) {
            event = bundle.getParcelable("event");
            imageUrl = bundle.getString("imageUrl");
            showEvent(event);
        }
    }

    private void showEvent(Event e) {
        TextView event_date = (TextView) findViewById(R.id.event_date);
        TextView event_desc = (TextView) findViewById(R.id.event_desc);
        TextView event_title = (TextView) findViewById(R.id.event_title);
        TextView event_location = (TextView) findViewById(R.id.event_location);
        TextView event_address = (TextView) findViewById(R.id.event_address);
        TextView event_time = (TextView) findViewById(R.id.event_time);
        ImageView event_image = (ImageView) findViewById(R.id.event_image);
        event_desc.setText(e.getDescription());
        event_date.setText(e.getDate());
        event_time.setText(e.getTime());
        event_address.setText(e.getAddress());
        event_location.setText(e.getLocation());
        event_title.setText(e.getTitle());
        if (imageUrl.length() > 4) {
            Glide.with(this).load(imageUrl).into(event_image);
            event.setImageUrl(imageUrl);
        }

        if (e.getFavorite()==1){
            star.setImageResource(R.drawable.checked);
            isChecked = true;
        } else {
            star.setImageResource(R.drawable.unchecked);
            isChecked = false;
        }
    }

    private void addEvent() {
        DBHelper dbHelper = new DBHelper(this);
        Dao<Event, Integer> dao = dbHelper.getEventDao();

        if (dao != null){
            try {
                event.setFavorite(1);
                dao.createOrUpdate(event);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void checkButton(View view){
        if(isChecked){
            star.setImageResource(R.drawable.unchecked);
            isChecked = false;
        } else {
            star.setImageResource(R.drawable.checked);
            isChecked = true;
        }

    }

    private void deleteEvent() {
        DBHelper dbHelper = new DBHelper(this);
        Dao<Event, Integer> dao = dbHelper.getEventDao();

        try {
            event.setFavorite(0);
            dao.delete(event);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showOnMap(View view){
        Intent intent = new Intent(this, MapsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("latitude", event.getLatitude());
        bundle.putString("longitude", event.getLongitude());
        bundle.putString("title", event.getTitle());
        intent.putExtra(mapsBundle, bundle);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isChecked){
            if (event.getFavorite() == 0) {
                addEvent();
                Toast.makeText(this, R.string.fav_saved, Toast.LENGTH_SHORT).show();
            }
        }else{
            if (event.getFavorite() == 1) {
                deleteEvent();
                Toast.makeText(this, R.string.fav_del, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void share(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Einladung zu " + event.getTitle());
        sendIntent.putExtra(Intent.EXTRA_TEXT,event.toString());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void browse(View view) {
        Uri uri = Uri.parse(event.getUrl());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
