package com.htwk.lfischer.letsfetz.async;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.htwk.lfischer.letsfetz.logic.EventsLogic;
import com.htwk.lfischer.letsfetz.model.Event;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Laura on 08.01.2017.
 */

public class EventLoader extends AsyncTaskLoader<List<Event>> {
    private String city;
    private String date;

    public EventLoader(Context context, String city, String date) {
        super(context);
        this.city = city;
        this.date = date;
    }

    private static final String TAG = EventLoader.class.getSimpleName();

    @Override
    public List<Event> loadInBackground() {
        try {
            return EventsLogic.loadWithRetrofit(city, date);
        } catch (Exception e) {
            Log.e(TAG,e.getMessage(),e);
            return new LinkedList<>();
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();     //otherwise the new loader wouldn't start
    }
}
