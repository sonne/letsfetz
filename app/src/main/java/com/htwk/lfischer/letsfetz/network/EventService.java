package com.htwk.lfischer.letsfetz.network;

import com.htwk.lfischer.letsfetz.model.Event;
import com.htwk.lfischer.letsfetz.model.ResponseEvent;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Laura on 08.01.2017.
 */

public interface EventService {
    @GET("events/search?app_key=TRNhRrdjTtQhh6S8")
    Call<ResponseEvent> getEvents(@Query("location") String location, @Query("date") String date, @Query("page_size") String size);
}