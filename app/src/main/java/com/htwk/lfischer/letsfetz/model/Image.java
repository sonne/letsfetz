package com.htwk.lfischer.letsfetz.model;

/**
 * Created by Laura on 26.01.2017.
 */

public class Image {

    public Medium medium;

    public Medium getMedium() {
        return medium;
    }

    public class Medium {
        public String url;

        public String getUrl() {
            if (url != null) {
                return url;
            } else {
                return "";
            }
        }
    }

}