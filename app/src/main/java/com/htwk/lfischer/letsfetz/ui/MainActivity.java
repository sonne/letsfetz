package com.htwk.lfischer.letsfetz.ui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.htwk.lfischer.letsfetz.R;
import com.htwk.lfischer.letsfetz.util.OnItemClickListener;
import com.htwk.lfischer.letsfetz.model.Event;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnItemClickListener<Event> {

    private Spinner cat_spinner;
    private ArrayAdapter<CharSequence> cat_adapter;
    private Calendar calendar = Calendar.getInstance();
    private Button setDate;
    private String format = "dd.MM.yyyy";
    private SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.GERMAN);
    public String selectedDate = sdf.format(Calendar.getInstance().getTime());
    private String selectedCity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setDate = (Button) findViewById(R.id.date);

        cat_spinner = (Spinner) findViewById(R.id.category);
        cat_adapter = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item);
        cat_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cat_spinner.setAdapter(cat_adapter);
        cat_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = (String) parent.getItemAtPosition(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager conManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public void setDate(View view){

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener(){

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateButton();
            }
        };

        DatePickerDialog pickerDia = new DatePickerDialog(this, date , calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        Calendar minCal = Calendar.getInstance();
        minCal.add(Calendar.DAY_OF_MONTH, -30);
        pickerDia.getDatePicker().setMinDate(minCal.getTimeInMillis());
        pickerDia.show();
    }

    private void updateDateButton() {
        selectedDate = sdf.format(calendar.getTime());
        setDate.setText(selectedDate);
    }

    public void load(View view){
        if (isNetworkAvailable()) {
            Intent intent = new Intent(this, SecondActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("city", selectedCity);
            bundle.putString("date", selectedDate);
            intent.putExtra("parameters", bundle);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Kein Internetzugriff", Toast.LENGTH_LONG).show();
        }
    }

    public void favs(View view){
        Intent intent = new Intent(this, FavoriteActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClick(Event item) {
        Toast.makeText(this,item.getTitle() + "", Toast.LENGTH_SHORT).show();
    }
}
