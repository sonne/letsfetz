package com.htwk.lfischer.letsfetz.util;

/**
 * Created by Laura on 08.01.2017.
 */

public interface OnItemClickListener<T> {
    void onItemClick(T item);
}
