package com.htwk.lfischer.letsfetz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laura on 18.01.2017.
 */

public class ResponseEvent{

    private Ev events;
    public Ev getEvents() {
        return events;
    }

    public class Ev {
        List<Event> event;
        public List<Event> getEvent() {
            return event;
        }

    }
}
