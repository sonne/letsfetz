package com.htwk.lfischer.letsfetz.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.htwk.lfischer.letsfetz.logic.DBHelper;
import com.htwk.lfischer.letsfetz.R;
import com.htwk.lfischer.letsfetz.util.OnItemClickListener;
import com.htwk.lfischer.letsfetz.model.Event;
import com.htwk.lfischer.letsfetz.ui.adapter.FavAdapter;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class FavoriteActivity extends AppCompatActivity implements OnItemClickListener<Event>{
    private RecyclerView eventsRecyclerView;
    private RecyclerView.Adapter fav_adapter;
    public static String bundleName = "selectedEvent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        fillRecyclerView();
    }

    private void fillRecyclerView() {
        eventsRecyclerView = (RecyclerView) findViewById(R.id.eventsRecycler);
        eventsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        fav_adapter = new FavAdapter(getFavEvents(), this);
        eventsRecyclerView.setAdapter(fav_adapter);
        if (getFavEvents().size() == 0){
            Toast.makeText(this, R.string.no_favs , Toast.LENGTH_LONG).show();
        }
    }

    public List<Event> getFavEvents(){
        DBHelper dbHelper = new DBHelper(this);
        Dao<Event, Integer> dao = dbHelper.getEventDao();
        try {
            List<Event> favList = dao.queryForAll();
            Collections.sort(favList);
            return favList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new LinkedList<>();
    }

    @Override
    public void onItemClick(Event item) {
        Intent intent = new Intent(this, EventActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("imageUrl", item.getImageUrl());
        bundle.putParcelable("event", item);
        intent.putExtra(bundleName, bundle);
        startActivity(intent);
    }

    public void update(View view) {
        fillRecyclerView();
    }
}
