package com.htwk.lfischer.letsfetz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.htwk.lfischer.letsfetz.R;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Laura on 08.01.2017.
 */
@DatabaseTable(tableName = "events")
public class Event implements Parcelable, Comparable<Event>{
    @DatabaseField(columnName = "_ID", id = true)
    private String id;

    @DatabaseField(columnName = "TITLE")
    private String title;

    @DatabaseField(columnName = "VENUE_NAME")
    private String venue_name;

    @DatabaseField(columnName = "START_TIME")
    private String start_time;

    @DatabaseField(columnName = "DATE")
    private String date;

    @DatabaseField(columnName = "FAVORITE")
    private int favorite;

    @DatabaseField(columnName = "VENUE_ADDRESS")
    private String venue_address;

    @DatabaseField(columnName = "LATITUDE")
    private String latitude;

    @DatabaseField(columnName = "LONGITUDE")
    private String longitude;

    @DatabaseField(columnName = "URL")
    private String url;

    @DatabaseField(columnName = "IMAGE_URL")
    private String imageUrl;

    private Image image;

    //fuer die DB
    public Event(){}

    public Event(String id, String title, String location, String time, String date,
                 String venue_address, String latitude, String longitude, int favorite, String url, String imageUrl) {
        this.id = id;
        this.title = title;
        this.venue_name = location;
        this.start_time = time;
        this.date = date;
        this.venue_address = venue_address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.favorite = favorite;
        this.url = url;
        this.imageUrl = imageUrl;
    }

    protected Event(Parcel in) {
        id = in.readString();
        title = in.readString();
        venue_name = in.readString();
        start_time = in.readString();
        date = in.readString();
        venue_address = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        favorite = in.readInt();
        url = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public void setId(String id) {
        this.id = id;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public void setFavorite(int favorite) { this.favorite = favorite; }
    public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

    public String getLongitude() {
        return longitude;
    }
    public String getLatitude() {
        return latitude;
    }
    public String getId() { return id; }
    public String getTitle() {
        String array[] = title.split("-");
        return array[0].trim(); }
    public String getDescription(){
        String array[] = title.split("-");
        if (array.length > 1){
            return array[1].trim();
        } else { return "";}
    }
    public String getLocation() {
        return venue_name;
    }
    public String getTime() {
        String[] timeNdate = start_time.split(" ");
        String time = timeNdate[1].substring(0,5);
        return time;
    }
    public String getDate() {
        String[] timeNdate = start_time.split(" ");
        String[] onlyDate = timeNdate[0].split("-");
        return onlyDate[2]+"."+onlyDate[1]+"."+onlyDate[0];
    }

    public String getStart_time() {
        return start_time;
    }

    public String getAddress(){ return venue_address; }
    public int getFavorite() { return favorite; }
    public String getUrl() { return url; }
    public String getImageUrl() { return ""+imageUrl; }
    public Image getImage(){ return image; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(venue_name);
        dest.writeString(start_time);
        dest.writeString(date);
        dest.writeString(venue_address);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeInt(favorite);
        dest.writeString(url);
        dest.writeString(imageUrl);
    }

    @Override
    public int compareTo(Event compEvent){
        //2017-01-30 20:00:00 to 201701302000
        //s.replaceAll("[\\s\\.]", "_")
        String prepareTime = compEvent.getStart_time().replace("-", "").replace(" ","").replace(":","");
        int compTime = Integer.parseInt(prepareTime.substring(2,12));
        prepareTime = this.start_time.replace("-", "").replace(" ","").replace(":","");
        int time = Integer.parseInt(prepareTime.substring(2,12));
        return (time - compTime);
    }

    @Override
    public String toString() {
        String text = "Hey, schau dir mal diese Veranstaltung an:" + " \n" +
                "\n" + this.title +
                "\n" + "--------------" +
                "\n" + this.venue_name +
                "\n" + this.getDate() +
                "\n" + this.getTime() +
                "\n\n"+ "Mehr Infos: " + this.url;
        return text;
    }
}

