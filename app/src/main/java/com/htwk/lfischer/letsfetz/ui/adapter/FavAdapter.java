package com.htwk.lfischer.letsfetz.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.htwk.lfischer.letsfetz.R;
import com.htwk.lfischer.letsfetz.util.OnItemClickListener;
import com.htwk.lfischer.letsfetz.model.Event;

import java.util.List;

/**
 * Created by Laura on 08.01.2017.
 */

public class FavAdapter extends RecyclerView.Adapter<FavAdapter.EventHolder> {
    private List<Event> events;
    private OnItemClickListener<Event> itemClickListener;

    public FavAdapter(List<Event> events, OnItemClickListener<Event> itemClickListener) {
        this.events = events;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite,parent,false);
        return new EventHolder(v);
    }

    @Override
    public void onBindViewHolder(EventHolder holder, int position) {
        final Event event = events.get(position);

        holder.title.setText(event.getTitle());
        holder.location.setText(event.getLocation());
        holder.time.setText(event.getTime());
        holder.date.setText(event.getDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(event);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return events.size();
    }

    public class EventHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView location;
        public TextView time;
        public TextView date;

        public EventHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            location = (TextView) itemView.findViewById(R.id.location);
            time = (TextView) itemView.findViewById(R.id.time);
            date = (TextView) itemView.findViewById(R.id.date);
        }
    }
}
